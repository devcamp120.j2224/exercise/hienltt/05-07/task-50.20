import com.devcamp.j50_javabasic.s10.NewDevcampApp;

public class App {
    public static void main(String[] args) throws Exception {
        /*
         * Comment nhiều dòng (khối)
         * ví dụ sử dụng biên string
         */
        System.out.println("Hello, World!");
        String strName = new String("Devcamp");
        System.out.println(strName);//comment 1 dòng: in biên strName ra console
        /**
         * Ví dụ sử dụng các phương thức của lớp String
         */
        System.out.println("Chuyen ve chu thuong: ToLowerCase: " + strName.toLowerCase());
        System.out.println("Chuyen ve chu hoa: ToUpperCase: " + strName.toUpperCase());
        System.out.println("Chieu dai cua chuoi: length: " + strName.length());

        /*
         * Goi phuong thuc cua lop khac
         */
        //phuong thuc tinh => goi truc tiep TenLop.TenPhuongThuc
        NewDevcampApp.name(30, "Tran Van Thang");

        //Phuong thuc thuong => tao doi tuong roi moi goi phuong thuc
        NewDevcampApp demo = new NewDevcampApp();
        demo.name("Tran Van Thang");
        String strTmp = demo.name("Nguyen Van Thuan", 35);
        System.out.println(strTmp);
    }
 }
 
