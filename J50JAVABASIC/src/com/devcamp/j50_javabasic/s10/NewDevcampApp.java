package com.devcamp.j50_javabasic.s10;

public class NewDevcampApp {
    public static void main(String[] args) {
        System.out.println("New Demo Java Class");

        NewDevcampApp.name(30, "Lam Tran The Hien");//goi phuong thuc tinh: dung truc tiep Class.TenPhuongThuc

        /**
         * Goi phuong thuc thuong: dung qua doi tuong (khoi tao => goi phuong thuc)
         */
        NewDevcampApp demo = new NewDevcampApp();
        demo.name("Lam The Hien");
        String strTmp = demo.name("Lam T.T Hien", 35);
        System.out.println(strTmp);
    }

    public void name(String strName) {
        System.out.println(strName);  
    }

    public String name(String strName, int age) {
        String strResult = "My name is " + strName + ", my age is " + age;
        return strResult;  
    }

    public static void name(int age, String strName) {
        String strResult = "My name is " + strName + ", my age is " + age;
        System.out.println(strResult);
    }
}
